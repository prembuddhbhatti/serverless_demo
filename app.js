const getDemo = async () => {
	await (await fetch("/.netlify/functions/demo")).json();
};
const getWelcome = async () => {
	await (await fetch("/.netlify/functions/getWelcome")).json();
};

getDemo();
getWelcome();
