exports.handler = function (event, context, callback) {
	if (event.body) {
		const { name } = JSON.parse(event.body);
		callback(null, {
			statusCode: 200,
			body: JSON.stringify({ msg: `Welcome ${name} to serverless world` }),
		});
	} else {
		callback(null, {
			statusCode: 200,
			body: JSON.stringify({ msg: `please provide a name` }),
		});
	}
};
